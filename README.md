# README #

This repo provides a range of code for vegetation community analysis.  

These tools were developed through a collaboration between the University of New South Wales and The New South Wales Office of Environment and Heritage.  

More details about the project can be viewed here:  
http://goo.gl/cIIpeS  

This conference paper from the Int. Ass. Vegetation Science Symposium 2014 outlines the key motivation and rational for this collaboration, watch out for the forthcoming papers!  
https://bitbucket.org/mitchest/unsw_oeh_veganalysis/downloads/Lyons_IAVS2014_abstract.pdf

### Running code ###

* Head over to the 'source' tab on the left to access the code
* All code and files should be cloned/downloaded to a single path
* Some scripts need to be updated with working directories, and most scripts will need file names updated

### Contribution guidelines ###

* Please add code if there is other analysis or plotting you would like to see
* Code will continue to be added and updated

### Contact ###

* Mitchell Lyons
* mitchell.lyons@gmail.com / mitchell.lyons@unsw.edu.au